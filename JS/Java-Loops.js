"use strict"

/** TODO: While Loops
 *   Create a while loop that uses console.log() to create the output shown below
 *   2
 *   4
 *   8
 *   16
 *   32
 *   64
 *   128
 *   256
 *   512
 *   1024
 *   2048
 *   4096
 *   8192
 *   16384
 *   32768
 *   65536
 */

/*var Num = 2

while (Num <= 65536) {
    Num *= 2
    console.log("#" + Num)
}*/

var Num = 2
while (Num <= 65536) {
    console.log(Num)
    Num *= 2;
}




/** TODO: For Loops
 *   Create a function name showMultiplicationTable that accepts a number and console.logs the multiplication table for that number
 *   Example:
 *   showMultiplicationTable(8) should output
 *   8 x 1 = 8
 *   8 x 2 = 16
 *   8 x 3 = 24
 *   8 x 4 = 32
 *   8 x 5 = 40
 *   8 x 6 = 49
 *   8 x 7 = 56
 *   8 x 8 = 64
 *   8 x 9 = 72
 *   8 x 10 = 80
 */



/*for (var x = 8; x <= 80; x *= y) {
    var y = 1

    if(x <= 80) {
        y += 1
    }

    console.log("for loop #" + x);
}*/

function showMultiplicationTable(Num) {
    for (var i = 1; i <= 10; i++) {
        //locol variable
        var answer = num * i;
        console.log(num + "x" + i + "=" + answer);
    }

}

showMultiplicationTable(10);
/** TODO: Create a for loop that uses console.log to create the output shown below.
 100
 95
 90
 85
 80
 75
 70
 65
 60
 55
 50
 45
 40
 35
 30
 25
 20
 15
 10
 5
 */

for (var A = 100; A >= 5; A -= 5 ) {


    console.log("#" + A)
}



/** TODO: Prompt the user for an odd number between 1 and 50. Use a loop and a break statement to continue prompting
 *   the user if they enter invalid input.Use a loop and the continue statement to output all the odd numbers between 1 and 50, except for the number the user entered.
 *   Output should look like this:
 Number to skip is: 27
 Here is an odd number: 1
 Here is an odd number: 3
 Here is an odd number: 5
 Here is an odd number: 7
 Here is an odd number: 9
 Here is an odd number: 11
 Here is an odd number: 13
 Here is an odd number: 15
 Here is an odd number: 17
 Here is an odd number: 19
 Here is an odd number: 21
 Here is an odd number: 23
 Here is an odd number: 25
 Yikes! Skipping number: 27
 Here is an odd number: 29
 Here is an odd number: 31
 Here is an odd number: 33
 Here is an odd number: 35
 Here is an odd number: 37
 Here is an odd number: 39
 Here is an odd number: 41
 Here is an odd number: 43
 Here is an odd number: 45
 Here is an odd number: 47
 Here is an odd number: 49
 */

/*var Number = prompt("Enter a number"); {

    if (Number > 50 ); {
    alert("bacon and cheese burgers are bomb")
    }
}*/
var userInput = prompt("enter a number between 1 and 50");
for (var T = 1; T < 50; T++)
{
    // if statement
    //prints out odd numbers only
    if (T % 2 === 0) {
        continue;
    }

    //if statement checks if T matches the userInput number
    if (T === userInput) {
        console.log("Yikes! skipping number" + T)
    }

    else {
        console.log("odd number" + T);
        //prints out T % 2 === 0
    }
}






/** TODO:
 *  Write a for loop that will iterate from 0 to 20. For each iteration, it will check if the current number is even or odd,
 *  and report that to the screen (e.g. "2 is even").
 */


for (var t = 0; t <= 20; t++) {
    if (t % 2 === 0) {
        console.log(t + "is even");
    }
    if (t % 2 !== 0) {
        console.log(t + "is odd");
    }
}









 /* TODO:
 *  Write a for loop that will iterate from 0 to 10. For each iteration of the for loop,
 *  it will multiply the number by 9 and log the result (e.g. "2 * 9 = 18").
 */

for (var L = 1; L >= 10; L++) {

    console.log(L + "* 9 =" * 9);
}











/** TODO: Break and ContinuePrompt the user for an odd number between 1 and 50. *
 *   Use a loop and a break statement to continue prompting the user if they enter invalid input.
 *   Use a loop and the continue statement to output all the odd numbers between 1 and 50, except for the number the user entered.
 */



/** BONUS:
 * Write a program that finds the summation of every number from 1 to num.
 * The number will always be a positive integer greater than 0.
 *
 * Ex.
 * summation(2) -> 3
 * 1 + 2
 *
 * summation(8) -> 36
 * 1 + 2 + 3 + 4 + 5 + 6 + 7 + 8
 */


var summation = function (num) {
    //local variable
    var total  = 0;

    // for loop
    for(var i = 1; i <= num; i++) {
        total += i;
    }
    return total;

};
console.log(summation(8))