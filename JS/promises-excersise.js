"use strict"

// Write a function name 'wait' that accepts a number as a parameter, and returns
// a PROMISE that RESOLVES AFTER THE PASSED NUMBER OF MILLISECONDS
// function wait(x) {
//
//     const mypromise = new Promise((resolve, reject, ) => {
//         setTimeout(() =>  {
//
//             if (x === 10) {
//                 resolve("its 10");
//             }
//             else {
//                 reject("its not 10");
//             }
//         });
//     });
// }
// const request = wait(10);
// const total = console.log(request); // pending promise

const wait = (delay) => {
    return new Promise(((resolve, reject) => {
        setTimeout(() => {
            resolve(`you will see this after${delay / 1000 } seconds`)
        })}))}
    wait(4000).then((message) => console.log(message));



// EX. wait(4000).then(() => console.log('You will see this after 4 seconds.'));





// EX. wait(8000).then(() => console.log('You will see this after 8 seconds.'));




//Exercise 2:
//Write a function testNum that takes a number as an argument and returns a Promise that tests if the value is less than or greater than the value 10.

const testNum = (i) => new Promise((c, d) => {
    if (i < 10) {
        c('Value is less than 10');
    }
    else {
        d('Value is greater than 10');
    }
});
testNum.then(data => console.log('', data));
testNum.catch(error => console.log('', error));}


//Exercise 3:
//Write two functions that use Promises that you can chain! The first function, makeAllCaps(), will take in an array of words and capitalize them, and then the second function, sortWords(), will sort the words in alphabetical order. If the array contains anything but strings, it should throw an error.

const myArray = ['apples', 'tomatoes', 'cucumber', 'bananas'];

const makeAllCaps = array => {
    return new promise(((resolve, reject) => {
    var arrayCaps = array.map(word => {
        if (typeof word === 'string') {
            return word.toUpperCase();
        }

        else {
        reject('error');
        }
    })
    resolve(arrayCaps);
}))
};


const sortWords = array => {
    return new promise(((resolve, reject) => {
        if (array) {
            array.forEach((ele) => {
                if (typeof ele !== 'string') {
                    reject('error!')
                }
            })
                    resolve(array.sort())
        }
        else {
            reject('error sorting words');
        }
    }))
};

makeAllCaps(myArray)
    .then(sortWords)
    .then((result) => console.log(result))
        .catch((error) => console.log(error));
