//OBJECTS, LOOPS, FUNCTIONS... OH MY!
    //New js file named objects-loops-functions.js
 "use strict"
const albums = [
    {artist: 'Michael Jackson', title: 'Thrillers', released: 1982, genre: 'Pop'},
    {artist: 'AC/DC', title: 'Back in Black', released: 1980, genre: 'Rock'},
    {artist: 'Pink Floyd', title: 'The Dark Side of the Moon', released: 1973, genre: 'Rock'},
    {artist: 'Bee Gees', title: 'Saturday Night Fever', released: 1977, genre: 'Disco'},
    {artist: 'Fleetwood Mac', title: 'Rumours', released: 1977, genre: 'Rock'},
    {artist: 'Shania Twain', title: 'Come On Over', released: 1997, genre: 'Country'},
    {artist: 'Michael Jackson', title: 'Bad', released: 1987, genre: 'Pop'},
    {artist: 'Led Zeppelin', title: 'Led Zeppelin IV', released: 1971, genre: 'Rock'},
    {artist: 'The Beatles', title: '1', released: 2000, genre: 'Rock'},
    {artist: 'Whitney Houston', title: 'Whitney', released: 1987, genre: 'Pop'},
    {artist: 'Def Leppard', title: 'Hysteria', released: 1987, genre: 'Rock'},
    {artist: 'Tupac', title: 'All Eyez on Me', released: 1996, genre: 'Rap'},
    {artist: 'Eminem', title: 'The Marshall Mathers LP', released: 2000, genre: 'Rap'},
    {artist: 'Green Day', title: 'Dookie', released: 1994, genre: 'Rock'},
    {artist: 'Michael Jackson', title: 'Dangerous', released: 1991, genre: 'Pop'},
    {artist: 'The Notorious B.I.G', title: 'Ready to Die', released: 1994, genre: 'Rap'},
    {artist: 'Adele', title: '21', released: 2011, genre: 'Pop'},
    {artist: 'Metallica', title: 'Load', released: 1996, genre: 'Rock'},
    {artist: 'Prince', title: '1999', released: 1982, genre: 'Pop'},
    {artist: 'Lady Gaga', title: 'Born This Way', released: 2011, genre: 'Pop'}]





/**
 * 1. create a for loop function that logs every 'Pop' album
 * 2. create a for each function that logs every 'Rock' album
 * 3. create a for each function that logs every album released before 2000
 * 4. create a for loop function that logs every album between 1990 - 2020
 * 5. for loop function that logs every Michael Jackson album
 * 6. create a function name 'addAlbum' that accepts the same parameters from 'albums' and add it to the array
 */

for (var i = 0; i <= albums.length; i++) {
    if (albums[i].genre === `pop`) {
        console.log(albums[i]);
    }
}


albums.forEach(function (album) {
    console.log(album.genre("rock"))
})

albums.forEach(function (date) {
    if ( date.released < 2000 ){
        console.log(date);
    }
});

for (var x = 0; 0 <= albums.length; x++) {
    if (albums[x].released >= 1990 && albums[x].released <= 2020) {
        console.log(albums[x]);
    }
}


for (var P = 0; P <= albums.length; P++) {
    if (albums[P].artist === "Michael Jackson") {
        console.log(albums[P].title)
    }
}


function addAlbum(artist, title, released, genre) {
albums.push
(

    {


        'artist': artist,
        'title': title,
        'released': released,
        'genre': genre

   }

   )
}
addAlbum();
console.log();
/**
 * TODO: Write a program that prints the numbers from 1 to 100.
 *  But for multiples of three print "Fizz" instead of the number
 *  and for the multiples of five print "Buzz".
 *  For numbers which are multiples of both three and five print "FizzBuzz".
 */

for (var number = 1; number <= 100; number ++) {

    if (number % 3 === 0 && number % 5 !== 0 ) {
        console.log("Fizz")
    }

    else if (number % 5 === 0 && number % 3 !== 0) {
        console.log('buzz')
    }

    else if (number % 3 === 0 && number % 5 === 0 ){
        console.log('fizzbuzz')
    }
    else {console.log(number)}
}



/**
 * TODO: Write a function that takes in two parameters
 *  and returns the sum of both parameters, then multiplies it by 5.
 */

function example(para1, para2){
    return (para1 + para2) * 5;
                  }
                  console.log(example(12,5));




