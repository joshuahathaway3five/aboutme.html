"use strict"
// jQuery Events
// Complete the following in a new js file named jquery-events-exercise.js
// NOTE: You'll also name a new html file, name this jquery-events-exercise.html
// Add jQuery code that will change the background color of a 'h1' element when clicked.
// Make all paragraphs have a font size of 18px when they are double clicked.
// Set all 'li' text color to green when the mouse is hovering,
// reset to black when it is not.
// Add an alert that reads "Keydown event triggered!" everytime a user pushes a key down
// Console log "Keyup event triggered!" whenever a user releases a key.
// BONUS: Create a form with one input with a type set to text.
// Using jQuery, create a counter for everytime a key is pressed.

$('#Click').click(function () {
    $(this).css('background-color', 'blue');
})

$('.Click2').dblclick(function () {
    $(this).css('font-size', '18px')
});

$('#UL').hover(
    function () {
        $(this).css('color', 'skyblue');
    },
    function () {
        $(this).css('color', 'black');
    }
);

$(document).keydown(function () {
    alert("keydown event triggered");
})

$(document).keyup(function () {
    console.log("keyup event was triggered")
});

$('#form-counter').keydown(function () {
   console.log(this.value.length);

    $('#counter').text(this,value,length);
});


