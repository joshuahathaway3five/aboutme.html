// ES6 EXERCISE
// Complete the following in a new js file name es6exercise.js
/*
 * Complete the TODO items below
 */
const developers = [
    {
        name: 'stephen',
        email: 'stephen@appddictionstudio.com',
        languages: ['html', 'css', 'javascript', 'php', 'java']
    },
    {
        name: 'karen',
        email: 'karen@appddictionstudio.com',
        languages: ['java', 'javascript', 'angular', 'spring boot']
    },
    {
        name: 'juan',
        email: 'juan@appddictionstudio.com',
        languages: ['java', 'aws', 'php']
    },
    {
        name: 'leslie',
        email: 'leslie@codebound.com',
        languages: ['javascript', 'java', 'sql']
    },
    {
        name: 'dwight',
        email: 'dwight@codebound.com',
        languages: ['html', 'angular', 'javascript', 'sql']
    }
];
// TODO: replace the `var` keyword with `const`, then try to reassign a variable as a const
// TODO: fill in your name and email and add some programming languages you know to the languages array
const name = `josh`
const email = 'joshuahathaway3five@yahoo.com'
const languages = [`html`, `css`, 'javascript']
// TODO: rewrite the object literal using object property shorthand
    name
    email
    languages

console.log(developers);
// TODO: replace `var` with `let` in the following variable declarations
let emails = [];
let names = [];
// TODO: rewrite the following using arrow functions
developers.forEach(developer => emails.push(developer.email));

developers.forEach(developer => name.push(developer.name))

developers.forEach(developer => languages.push(developer.languages))
// TODO: replace `var` with `const` in the following declaration
const developerTeam = [];

developers.forEach(function(developer) {

    // TODO: rewrite the code below to use object destructuring assignment
    //       note that you can also use destructuring assignment in the function
    //       parameter definition
    const {name, email, language} = developer;
    // TODO: rewrite the assignment below to use template strings
    developerTeam.push(name + '\'s email is ' + email + name + ' knows ' + languages.join(', '));
});

developerTeam.push(`'${name}'s emails name is ${email} ${name} knows ${languages}`)
console.log(developerTeam);

// TODO: Use `const` for the following variable
for (const ul of developers) {
    list += `<li>$(ul.name)</li>`
}

list += '</ul>'

document.write(list)



// TODO: rewrite the following loop to use a for..of loop
developers.forEach(function (developer) {
    // TODO: rewrite the assignment below to use template strings
//     list('<li>' + developer + '</li>', </ul>);
// // });
// // list += '</ul>';
// // document.write(list);
//












// Complete the following in a new js file named 'more-es6-drills.js'





const petName = 'Chula';
const age = 14;





// REWRITE THE FOLLOWING USING TEMPLATE STRINGS
// console.log("My dog is named " + petName +
//     ' and she is ' + age + ' years old.');

 console.log((`My dog is named ${petName} and she is ${age} years old`))})





// REWRITE THE FOLLOWING IN AN ARROW FUNCTION
// function addTen(num1) {
//     return num1 + 10;
// }

    const total = num1 => + 10;
    console.log(total(21))







// REWRITE THE FOLLOWING IN AN ARROW FUNCTION
// function minusFive(num1) {
//     return num1 - 5;
// }

const num2 = 5
const total2 = num2 => - 5;










// REWRITE THE FOLLOWING IN AN ARROW FUNCTION
// function multiplyByTwo(num1) {
//     return num1 * 2;
// }

const num3 = 1
const total = num3 => num3 * 2







// REWRITE THE FOLLOWING IN AN ARROW FUNCTION
// const greetings = function (name) {
//     return "Hello, " + name + ' how are you?';
// };











// REWRITE THE FOLLOWING IN AN ARROW FUNCTION
// function haveWeMet(name) {
//     if (name === 'Bob') {
//         return name + "Nice to see you again";
//     } else {
//         return "Nice to meet you"
//     }
// }

const havewemet = name => (name === 'bob') ? "nice to see you again" : "nice to meet you";








