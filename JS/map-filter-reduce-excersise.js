"use strict"

// EXERCISE
// MAP FILTER REDUCE EXERCISE
//Complete the following in a new js file name 'map-filter-reduce-exercise.js'



const developers = [
    {
        name: 'stephen',
        email: 'stephen@appddictionstudio.com',
        languages: ['html', 'css', 'javascript', 'angular', 'php'],
        yearExperience: 4
    },
    {
        name: 'karen',
        email: 'karen@appddictionstudio.com',
        languages: ['java', 'javascript', 'spring boot'],
        yearExperience: 3
    },
    {
        name: 'juan',
        email: 'juan@appddictionstudio.com',
        languages: ['html', 'css', 'java', 'aws', 'php'],
        yearExperience: 2
    },
    {
        name: 'leslie',
        email: 'leslie@codebound.com',
        languages: ['node', 'npm', 'sql', 'javascript', 'java' ],
        yearExperience: 5
    },
    {
        name: 'dwight',
        email: 'dwight@codebound.com',
        languages: ['html', 'angular', 'javascript', 'sql'],
        yearExperience: 8
    }
];
/**Use .filter to create an array of developer objects where they have
 at least 5 languages in the languages array*/
/**Use .map to create an array of strings where each element is a developer's
 email address*/
/**Use reduce to get the total years of experience from the list of developers.
 * Once you get the total of years you can use the result to calculate the average.*/
/**Use reduce to get the longest email from the list.*/
/**Use reduce to get the list of developer's names in a single string
 - output:
 CodeBound Staff: stephen, karen, juan, leslie, dwight*/
/** BONUS: use reduce to get the unique list of languages from the list
 of developers
 */

const developer5length = developers.filter(function (developer) {
    if (developer.languages.length >= 5) {
        return developer
    }
});
    console.log(
        developer5length
    )



// const emails = developer.map(num => num.emails)
// console.log(emails);






const years = developers.reduce((a, b) => {
    return a + b.yearExperience

}, 0);

// average

const average = years / developers.length;
console.log(average)



const longestemail = developers.reduce((a,b) => {

    return a.email.length > b.email.length ? a : b;
}).email;
console.log(longestemail);



const shortestemail = developers.reduce((a,b) => {

    return a.email.length < b.email.length ? a : b;
}).email;
console.log(shortestemail);

const DevNames = developers.reduce((accumulation, person) => {

    return accumulation + person.name + ",";
}, "");
console.log()







'use strict';
const companies = [
    {name: 'Walmart', category: 'Retail', start_year: 1962, location: { city: 'Rogers', state: 'AR' }},
    {name: 'Microsoft', category: 'Technology', start_year: 1975, location: { city: 'Albuquerque', state: 'NM' }},
    {name: 'Target', category: 'Retail', start_year: 1902, location: { city: 'Minneapolis', state: 'MN' }},
    {name: 'Wells Fargo', category: 'Financial', start_year: 1852, location: { city: 'New York', state: 'NY' }},
    {name: 'Amazon', category: 'Retail', start_year: 1994, location: { city: 'Bellevue', state: 'WA' }},
    {name: 'Capital One', category: 'Financial', start_year: 1994, location: { city: 'Richmond', state: 'VA' }},
    {name: 'CitiBank', category: 'Financial', start_year: 1812, location: { city: 'New York', state: 'NY' }},
    {name: 'Apple', category: 'Technology', start_year: 1976, location: { city: 'Cupertino', state: 'CA' }},
    {name: 'Best Buy', category: 'Retail', start_year: 1966, location: { city: 'Richfield', state: 'MN' }},
    {name: 'Facebook', category: 'Technology', start_year: 2004, location: { city: 'Cambridge', state: 'MA' }},
];
// filter all of the company names
// const filterComp = companies.filter(function (Company) {
//     if (Company.name.length > 0) {
//         return Company
//
//     }}, "")
// console.log(filterComp)

const filtercomp = companies.map(c => c.name);
console.log(filtercomp)


// filter all the company names started before 1990

const filtercomp1990 = companies.filter (c => c.name && c.start_year < 1990 );
console.log(filtercomp1990)



// filter all the retail companies

const filterretail = companies.filter (c => c.category === 'Retail')
console.log(filterretail)

// find all the technology and financial companies

const filter4 = companies.filter (c => c.category === 'Technology' || c.category === 'Financial')
console.log(filter4)

// filter all the companies that are located in the state of NY

const filter5 = companies.filter (c => c.location.state === 'NY')
console.log(filter5)

// find all the companies that started on an even year.
const filter6 = companies.filter(c => c.start_year % 2 === 0);

// use map and multiply each start year by 2
const filter7 = companies.map (c => c.start_year * 2);
console.log(filter7)
// find the total of all the start year combined.

const totalstartyear = companies.reduce((total, store) => {
    return total + store.start_year;

}, 0)

// display all the company names in alphabetical order

const companynames = companies.map(x => x.name);
companynames.sort()
console.log(companynames)

// display all the company names for youngest to oldest.

const filter8 = companies.map(x => x.start_year)
filter.sort();
const reverseFilter = filter8.reverse()
console.log(reverseFilter)